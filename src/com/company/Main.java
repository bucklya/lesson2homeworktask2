package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Эта программа предназначенна для решения уровнений типа ax^2 + bx + c = 0");
        System.out.println("Введите А: ");
        Scanner numberInputA = new Scanner(System.in);
        double numberA = numberInputA.nextDouble();
        System.out.println("Введите B: ");
        Scanner numberInputB = new Scanner(System.in);
        double numberB = numberInputB.nextDouble();
        System.out.println("Введите C: ");
        Scanner numberInputC = new Scanner(System.in);
        double numberC = numberInputC.nextDouble();

        // System.out.println(numberA +" "+ numberB +" "+ numberC);

        if (numberA == 0) {
            double numberAnswer = (-numberC / numberB);
            System.out.printf("Ответ: %.3f \n", numberAnswer);
        } else if (numberB == 0) {
            if (numberC > 0) {
                System.out.println("Решения нет, т.к. квадрат не может быть равен отрицательному числу.");
            } else {
                double numberAnswerOne = Math.sqrt(-numberC / numberA);
                double numberAnswerTwo = -(Math.sqrt(-numberC / numberA));
                System.out.printf("Ответ: %.3f и %.3f \n", numberAnswerOne, numberAnswerTwo);
            }
        } else if (numberC == 0) {
            double numberAnswer = -(numberB / numberA);
            System.out.printf("Ответ: 0 и %.3f \n", numberAnswer);
        } else {
            double discriminant = (numberB * numberB - 4 * numberA * numberC);
            double discriminantSQRT = Math.sqrt(discriminant);
            if (discriminant > 0) {
                double numberAnswerOne = (-numberB + discriminantSQRT) / (2 * numberA);
                double numberAnswerTwo = (-numberB - discriminantSQRT) / (2 * numberA);
                System.out.printf("Ответ: %.3f и %.3f \n", numberAnswerOne, numberAnswerTwo);
            } else if (discriminant == 0) {
                double numberAnswer = (-numberB + discriminantSQRT) / (2 * numberA);
                System.out.printf("Ответ: %.3f \n", numberAnswer);
            } else {
                System.out.println("Дискриминант меньше нуля, решений для данного уровнения нет.");
            }
        }
    }
}


